import java.util.Random;

public class MazeGenerator {
    private char[][] map;
    private int[] Intrance;
    private int[] Exit;
    int width;
    int height;

    public MazeGenerator(int width,int height){
        this.width=width;
        this.height=height;
        this.map=new char[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if(i%2==0 || j%2==0){
                    map[i][j]='#';
                }else{
                   map[i][j]=' '; 
                }  
            }
        }
        this.Intrance=generateSpecialTile("in");
        this.Exit=generateSpecialTile("out");
    }

    private int[] generateSpecialTile(String type){
        Random r= new Random();
        int decideur= r.nextInt(100);
        int[] res =new int[2];
        int neutralValue=0;
        switch (type) {
            case "in":
                neutralValue=0;
                break;
            case "out":
                if(decideur%2==0){
                    neutralValue=height-1;
                }else{
                    neutralValue=width-1;
                }
                break;
            default:
            throw new RuntimeException();
        }
        
        if (decideur%2==0) {
            res[0]=neutralValue;
            res[1]=((r.nextInt(width-2)/2)*2)+1;
        }else{
            res[0]=((r.nextInt(height-2)/2)*2)+1;
            res[1]=neutralValue;
        }
        changeWallToPath(res);
        return res;
    }

    public void createChemin(int[] first,int[] second){
        if((first[0]==second[0]&&(first[1]-second[1]==-2 ||first[1]-second[1]==2))||(first[1]==second[1]&&(first[0]-second[0]==-2 ||first[0]-second[0]==2))){
            this.map[(first[0]+second[0])/2][(first[1]+second[1])/2]= ' ';
        }
    }

    private void changeWallToPath(int[] coords){
        this.map[coords[0]][coords[1]]=' ';
    }

    public char[][] getMap(){
        return this.map;
    }   
    public int[] getIn(){
        return Intrance;
    }

    public int[] getOut(){
        return Exit;
    }
}
