
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;


public class CreationTest {
    @Test
    public void testCreationCell(){
        MazeGenerator mg= new MazeGenerator(3,3);
        assertEquals(mg.getMap()[mg.getIn()[0]][mg.getIn()[1]], ' ');
        assertNotEquals(mg.getMap()[mg.getIn()[0]][mg.getIn()[1]], '#');
    }

    @Test
    public void testCreationGrid(){
        MazeGenerator mg= new MazeGenerator(3, 3);
        if(mg.getIn()[0]==0){
            assertNotEquals(0, mg.getIn()[1]);
            assertEquals(mg.height, mg.getIn()[1]);
        }else{
            assertNotEquals(0, mg.getIn()[0]);
            assertEquals(mg.width, mg.getIn()[0]);
        }
        assertNotEquals(mg.getIn()[0], mg.getIn()[1]);
    }

    @Test
    public void testCreationMur(){
        MazeGenerator mg= new MazeGenerator(5, 5);
        assertEquals('#',mg.getMap()[0][0]);
        assertEquals('#', mg.getMap()[2][2]);
        assertEquals('#',mg.getMap()[0][2]);
        assertEquals('#', mg.getMap()[2][4]);
    }

    @Test
    public void testCreationChemin(){
        MazeGenerator mg =new MazeGenerator(5, 5);
        assertEquals(' ', mg.getMap()[1][1]);
        assertEquals('#', mg.getMap()[1][2]);
        mg.createChemin(new int[]{1,1},new int[]{1,3});
        assertEquals(' ', mg.getMap()[1][1]);
        assertEquals(' ', mg.getMap()[1][2]);
        
    }
    
}
